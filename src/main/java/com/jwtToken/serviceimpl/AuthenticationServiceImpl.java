package com.jwtToken.serviceimpl;

import com.jwtToken.dto.JwtAuthenticationResponse;
import com.jwtToken.dto.SignInRequest;
import com.jwtToken.dto.SignUpRequest;
import com.jwtToken.entities.User;
import com.jwtToken.repositories.UserRepository;
import com.jwtToken.services.AuthenticationService;
import com.jwtToken.services.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;


@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

  private final UserRepository userRepository;
  private final PasswordEncoder passwordEncoder;
  private final AuthenticationManager authenticationManager;
  private final JWTService jwtService;

  public User signUp(SignUpRequest signUpRequest) {
    User user = new User();
    user.setUsername(signUpRequest.getUsername());
    user.setRole(signUpRequest.getRole());
    user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

    return userRepository.save(user);
  }

  public JwtAuthenticationResponse signIn(SignInRequest signInRequest) {
    this.authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword()));

    var user = userRepository.findByUsername(signInRequest.getUsername())
        .orElseThrow(() -> new IllegalArgumentException("Invalid username or password"));

    var jwt = jwtService.generateToken(user);

    JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();
    jwtAuthenticationResponse.setToken(jwt);
    jwtAuthenticationResponse.setUsername(user.getUsername());

    return jwtAuthenticationResponse;
  }

  public JwtAuthenticationResponse refreshToken(SignInRequest signInRequest) {
    this.authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPassword()));

    var user = Objects.requireNonNull(userRepository.findByUsername(signInRequest.getUsername())
        .orElseThrow(() -> new IllegalArgumentException("Invalid username or password")));

    var jwt = jwtService.generateToken(user);

    JwtAuthenticationResponse jwtAuthenticationResponse = new JwtAuthenticationResponse();
    jwtAuthenticationResponse.setToken(jwt);
    jwtAuthenticationResponse.setUsername(user.getUsername());

    return jwtAuthenticationResponse;
  }

}
