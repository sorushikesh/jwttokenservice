package com.jwtToken.config.security;

import com.jwtToken.services.JWTService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JWTAuthenticationFilter extends OncePerRequestFilter {

  private static final String BEARER_PREFIX = "Bearer ";

  private final JWTService jwtService;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain
  ) throws ServletException, IOException {

    final String requestHeader = request.getHeader("Authorization");
    final String jwt;

    if (requestHeader != null && requestHeader.startsWith(BEARER_PREFIX)) {
      jwt = requestHeader.substring(BEARER_PREFIX.length());
      String username = this.jwtService.extractUsername(jwt);

      if (!username.isEmpty() && SecurityContextHolder.getContext().getAuthentication() == null) {
        this.jwtService.validateAndSetAuthentication(request, username, jwt, response);
      }

      filterChain.doFilter(request, response);
    } else {
      filterChain.doFilter(request, response);
    }
  }
}
