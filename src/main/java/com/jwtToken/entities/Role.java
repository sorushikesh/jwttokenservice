package com.jwtToken.entities;

public enum Role {
  USER,
  ADMIN
}
