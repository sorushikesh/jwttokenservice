package com.jwtToken.services;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.IOException;

public interface JWTService {

  /**
   * Extract the username from a JWT token.
   *
   * @param token The JWT token.
   * @return The username extracted from the token.
   */
  String extractUsername(String token);

  /**
   * Generate a JWT token for the provided user details.
   *
   * @param userDetails The user details for whom the token is generated.
   * @return The generated JWT token.
   */
  String generateToken(UserDetails userDetails);


  /**
   * Validate the provided token, set authentication in the response, and handle validation.
   *
   * @param request The HTTP servlet request.
   * @param username The username associated with the token.
   * @param token The JWT token.
   * @param response The HTTP servlet response.
   * @throws IOException If an I/O error occurs during validation.
   */
  void validateAndSetAuthentication(
      HttpServletRequest request, String username, String token, HttpServletResponse response
  ) throws IOException;

  /**
   * Validate a JWT token against user details.
   *
   * @param token The JWT token to validate.
   * @param userDetails The user details to validate against.
   * @return True if the token is valid for the provided user details, false otherwise.
   */
  Boolean validateToken(String token, UserDetails userDetails);
}
