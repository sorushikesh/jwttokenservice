package com.jwtToken;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JWTTokenServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(JWTTokenServiceApplication.class, args);
  }
}
