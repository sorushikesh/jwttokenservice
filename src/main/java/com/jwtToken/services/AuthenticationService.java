package com.jwtToken.services;


import com.jwtToken.dto.JwtAuthenticationResponse;
import com.jwtToken.dto.SignInRequest;
import com.jwtToken.dto.SignUpRequest;
import com.jwtToken.entities.User;

public interface AuthenticationService {

  /**
   * Register a new user.
   *
   * @param signUpRequest The request containing user details for registration.
   * @return The registered user.
   */
  User signUp(SignUpRequest signUpRequest);

  /**
   * Sign in and generate an authentication token.
   *
   * @param signInRequest The request containing user credentials for sign-in.
   * @return The authentication response containing a JWT token.
   */
  JwtAuthenticationResponse signIn(SignInRequest signInRequest);

  /**
   * Refresh the authentication token.
   *
   * @param signInRequest The request containing user credentials for token refresh.
   * @return The authentication response containing a new JWT token.
   */
  JwtAuthenticationResponse refreshToken(SignInRequest signInRequest);
}
