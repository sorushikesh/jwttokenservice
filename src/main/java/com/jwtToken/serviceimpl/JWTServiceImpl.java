package com.jwtToken.serviceimpl;

import com.jwtToken.services.JWTService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

@Service
public class JWTServiceImpl implements JWTService {

  public static final String SECRET = "B5679937556AF5CA9D221A6923E89135FEAAB12A82A67CF969C7B76149A75C86";
  private static final String BEARER_PREFIX = "Bearer ";
  private static final long JWT_TOKEN_VALIDITY = TimeUnit.HOURS.toSeconds(24);
  private final CustomUserDetailsService customUserDetailsService;

  @Autowired
  public JWTServiceImpl(CustomUserDetailsService customUserDetailsService) {
    this.customUserDetailsService = customUserDetailsService;
  }

  public String generateToken(UserDetails userDetails) {
    Map<String, Object> claims = new HashMap<>();
    return doGenerateToken(claims, userDetails);
  }

  private String doGenerateToken(Map<String, Object> claims, UserDetails userDetails) {
    return Jwts.builder().setSubject(userDetails.getUsername()).setIssuedAt(generateCurrentDate())
        .setExpiration(generateExpirationDate()).setHeader(generateHeader()).claim("claims", claims)
        .signWith(getSignKey(), SignatureAlgorithm.HS256).compact();
  }

  private Date generateCurrentDate() {
    return new Date(System.currentTimeMillis());
  }

  private Date generateExpirationDate() {
    return new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000);
  }

  private Key getSignKey() {
    byte[] key = Decoders.BASE64.decode(SECRET);
    return Keys.hmacShaKeyFor(key);
  }

  private Map<String, Object> generateHeader() {
    Map<String, Object> header = new HashMap<>();
    header.put("typ", "JWT");
    header.put("alg", "HS256");
    return header;
  }

  public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = extractAllClaims(token);
    return claimsResolver.apply(claims);
  }

  private Claims extractAllClaims(String token) {

    return Jwts.parser().setSigningKey(getSignKey()).build().parseClaimsJws(token).getBody();
  }


  public String extractUsername(String token) {
    return extractClaim(token, Claims::getSubject);
  }

  public void validateAndSetAuthentication(
      HttpServletRequest request, String username, String token, HttpServletResponse response
  ) throws IOException {
    UserDetails userDetails = this.customUserDetailsService.loadUserByUsername(username);
    if (validateToken(token, userDetails)) {
      setAuthentication(request, userDetails);
    } else {
      response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token validation fails");
    }
  }

  public Boolean validateToken(String token, UserDetails userDetails) {
    final String username = extractUsername(token);
    return username != null && userDetails != null && username.equals(userDetails.getUsername()) && !isTokenExpired(
        token);
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration != null && expiration.before(new Date());
  }

  public Date getExpirationDateFromToken(String token) {
    return extractClaim(token, Claims::getExpiration);
  }

  private void setAuthentication(HttpServletRequest request, UserDetails userDetails) {
    SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
    UsernamePasswordAuthenticationToken authentication =
        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
    securityContext.setAuthentication(authentication);
    SecurityContextHolder.setContext(securityContext);
  }
}
