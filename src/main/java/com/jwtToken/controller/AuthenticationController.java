package com.jwtToken.controller;

import com.jwtToken.dto.JwtAuthenticationResponse;
import com.jwtToken.dto.SignInRequest;
import com.jwtToken.dto.SignUpRequest;
import com.jwtToken.entities.User;
import com.jwtToken.services.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {

  private final AuthenticationService authenticationService;

  @PostMapping("/signup")
  public ResponseEntity<User> signup(@RequestBody SignUpRequest signUpRequest) {
    return ResponseEntity.ok(authenticationService.signUp(signUpRequest));
  }

  @PostMapping("/signin")
  public ResponseEntity<JwtAuthenticationResponse> signIn(@RequestBody SignInRequest signInRequest) {
    return ResponseEntity.ok(authenticationService.signIn(signInRequest));
  }

  @PostMapping("/token")
  public ResponseEntity<JwtAuthenticationResponse> getToken(@RequestBody SignInRequest signInRequest) {
    return ResponseEntity.ok().body(this.authenticationService.refreshToken(signInRequest));
  }
}
