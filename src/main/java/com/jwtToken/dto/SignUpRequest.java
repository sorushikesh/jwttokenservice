package com.jwtToken.dto;

import com.jwtToken.entities.Role;
import lombok.Data;

@Data
public class SignUpRequest {

  private String username;
  private String password;
  private Role role;
}
